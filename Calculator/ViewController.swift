//
//  ViewController.swift
//  Calculator
//
//  Created by Starceaker on 23/08/16.
//  Copyright © 2016 Starceaker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var brain = CalculatorBrain()
    
    @IBOutlet private weak var display: UILabel! //Implicitly unwraps each time it is accessed due to !
    @IBOutlet weak var formulaHistoryLabel: UILabel!
    
    private var userIsInTheMiddleOfTyping = false

    private var displayValue: Double{
        get{
            return Double(display.text!)!
        }
        set{
            display.text = newValue.cleanValue
        }
    }
    
    @IBAction private func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        
        setDigit(digit)
    }
    
    @IBAction private func addFloatingSeparator(_ sender: UIButton) {
        let separator = sender.currentTitle!
        
        if let currentValueText = display.text{
            let range = currentValueText.range(of: separator)
            if(range == nil){
                setDigit(separator)
            }
        }
    }
    
    private func setDigit(_ digit : String)
    {
        if userIsInTheMiddleOfTyping{
            let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + digit
        }
        else{
            //brain.clear() //CLINTTODO: this doesn't work yet. //When typing a digit just after a new operation that's the same as clearing. This is part "h."
            display.text = digit
        }

        userIsInTheMiddleOfTyping = true
    }
    
    @IBAction private func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping{
            brain.setOperand(operand: displayValue)
            userIsInTheMiddleOfTyping = false
        }
        
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(symbol: mathematicalSymbol)
        }
        
        displayValue = brain.accumulator
        formulaHistoryLabel.text = brain.formulaEnteredInCurrentSession
    }
}

	
