//
//  Extensions.swift
//  Calculator
//
//  Created by Starceaker on 31/08/16.
//  Copyright © 2016 Starceaker. All rights reserved.
//

import Foundation

extension Double{
    public var cleanValue : String{
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
