//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Starceaker on 24/08/16.
//  Copyright © 2016 Starceaker. All rights reserved.
//

import Foundation

/*func multiply(op1: Double, op2: Double) -> Double
{
    return op1 * op2
}*/

class CalculatorBrain
{
    //CLINTTODO: find the following key combinations and character
    /*
     
      ( ) []
    
     For the following group, use Windows key + arrows left and right
     HOME: start of line
     Shift HOME: select start of line
     END: end of line
     Shift END: ...
     
     look for the ‹ and > on the keyboard√
     */
    private var _accumulator = 0.0
    private var _formulaHistory = ""
    private var _operandToBeAppendedToHistory = ""
    
    private var operations: Dictionary<String, Operation> = [
        "π": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "cos": Operation.UnaryOperation(cos),
        "×": Operation.BinaryOperation({$0 * $1}),
        ":": Operation.BinaryOperation({$0 / $1}),
        "+": Operation.BinaryOperation({$0 + $1}),
        "-": Operation.BinaryOperation({$0 - $1}),
        "C": Operation.Clear, //Should we make this a clear func that also clears the pending operation? TODO
        "=": Operation.Equals,
        "abs": Operation.UnaryOperation(abs),
        "∓": Operation.UnaryOperation({$0 * -1})
        //"x!": Operation.UnaryOperation(log2(<#T##Double#>))
    ]
    
    var accumulator: Double{
        get{
            return _accumulator
        }
    }
    
    var isPartialResult: Bool{
        return pendingBinaryOperation != nil
    }
    
    //CLINTTODO: reread part 7 of assignment 1. The adding to description should only be done when using an operation, not when adding an operand. Also look into including brackets ()
    var formulaEnteredInCurrentSession: String{
        if(_formulaHistory.characters.count > 0)
        {
            if (isPartialResult){
                return _formulaHistory + "..."
            }
            else{
                return _formulaHistory + "="
            }
        }
        
        return ""
    }
    
    private var pendingBinaryOperation : PendingBinaryOperationInfo?
    
    func setOperand(operand: Double){
        _accumulator=operand
        _operandToBeAppendedToHistory = operand.cleanValue
    }
    
    func performOperation(symbol: String){
        if let operation = operations[symbol]{
            switch operation
            {
            case .Constant(let value): //This will work when entering a constant after setting an operand (it erases the operand) but when using multiple constants consecutively the history will show all these constants
                _formulaHistory += symbol
                _operandToBeAppendedToHistory = ""
                
                _accumulator = value
            case .UnaryOperation(let function):
                if(isPartialResult){
                    _formulaHistory += symbol + "(" + _accumulator.cleanValue + ")"
                }
                else{
                    _formulaHistory = symbol + "(" + _formulaHistory + ")"
                }
                _operandToBeAppendedToHistory = ""
                
                _accumulator = function(_accumulator)
                
            case .BinaryOperation(let function):
                _formulaHistory += _operandToBeAppendedToHistory + symbol
                
                executePendingBinaryOperation()
                pendingBinaryOperation = PendingBinaryOperationInfo(pendingOperation: function, previousOperand: _accumulator)
                
            case .Equals:
                _formulaHistory += _operandToBeAppendedToHistory
                executePendingBinaryOperation()
            case .Clear:
                clear()

            }
        }
    }
    
    private func executePendingBinaryOperation(){
        if(pendingBinaryOperation != nil)
        {
        let pending = pendingBinaryOperation!
        _accumulator = pending.pendingOperation(pending.previousOperand, _accumulator)
        pendingBinaryOperation = nil
        _operandToBeAppendedToHistory=""
        }
    }
    
    func clear()
    {
        pendingBinaryOperation = nil
        _accumulator = 0
        _formulaHistory = ""
    }
    
    //Find a way to store the selected BinaryOperation and the current number on screen, until we press another number and then press the equals.
    private struct PendingBinaryOperationInfo
    {
        var pendingOperation: (Double, Double) -> Double
        var previousOperand: Double
        
    }
    
    private enum Operation{
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
        case Clear
    }
    
}



/*
 //
 //  CalculatorBrain.swift
 //  Calculator
 //
 //  Created by Starceaker on 24/08/16.
 //  Copyright © 2016 Starceaker. All rights reserved.
 //
 
 import Foundation
 
 /*func multiply(op1: Double, op2: Double) -> Double
 {
 return op1 * op2
 }*/
 
 class CalculatorBrain
 {
 //CLINTTODO: find the following key combinations and character
 /*
 
 ( ) []
 
 For the following group, use Windows key + arrows left and right
 HOME: start of line
 Shift HOME: select start of line
 END: end of line
 Shift END: ...
 
 look for the ‹ and > on the keyboard√
 */
 private var _accumulator = 0.0
 private var _formulaHistory = ""
 private var _pendingEnteredCharacters = [String]()
 
 private var operations: Dictionary<String, Operation> = [
 "π": Operation.Constant(M_PI),
 "e": Operation.Constant(M_E),
 "√": Operation.UnaryOperation(sqrt),
 "cos": Operation.UnaryOperation(cos),
 "×": Operation.BinaryOperation({$0 * $1}),
 ":": Operation.BinaryOperation({$0 / $1}),
 "+": Operation.BinaryOperation({$0 + $1}),
 "-": Operation.BinaryOperation({$0 - $1}),
 "C": Operation.Clear, //Should we make this a clear func that also clears the pending operation? TODO
 "=": Operation.Equals,
 "abs": Operation.UnaryOperation(abs),
 "∓": Operation.UnaryOperation({$0 * -1})
 //"x!": Operation.UnaryOperation(log2(<#T##Double#>))
 ]
 
 var accumulator: Double{
 get{
 return _accumulator
 }
 }
 
 var isPartialResult: Bool{
 return pendingBinaryOperation != nil
 }
 
 //CLINTTODO: reread part 7 of assignment 1. The adding to description should only be done when using an operation, not when adding an operand. Also look into including brackets ()
 var formulaEnteredInCurrentSession: String{
 if(_formulaHistory.characters.count > 0)
 {
 if (isPartialResult){
 return _formulaHistory + "..."
 }
 else{
 return _formulaHistory + "="
 }
 }
 
 return ""
 }
 
 private var pendingBinaryOperation : PendingBinaryOperationInfo?
 
 func setOperand(operand: Double){
 _accumulator=operand
 }
 
 func performOperation(symbol: String){
 if let operation = operations[symbol]{
 /*if case Operation.Equals = operation{
 }
 else{
 addToDescription(symbol)
 }*/
 
 //CLINTTODO: how do I display the symbol π instead of the number in the label? Now the BinaryOperation
 //CLINTTODO: 7. f. g. h.
 //This is not correct for 7 + sqrt(9) = --> it will show 7 + sqrt(9)3 =
 switch operation
 {
 case .Constant(let value):
 _pendingEnteredCharacters.append(symbol)
 _accumulator = value
 case .UnaryOperation(let function):
 if(isPartialResult){
 _formulaHistory += symbol + "(" + _accumulator.cleanValue + ")"
 }
 else{
 _formulaHistory = symbol + "(" + _formulaHistory + ")"
 }
 
 _accumulator = function(_accumulator)
 case .BinaryOperation(let function):
 _formulaHistory += _accumulator.cleanValue + " " + symbol + " " //This is not correct for 7 + sqrt(9) + --> it will show 7 + sqrt(9)3 +
 executePendingBinaryOperation()
 pendingBinaryOperation = PendingBinaryOperationInfo(pendingOperation: function, previousOperand: _accumulator)
 case .Equals:
 _formulaHistory += _accumulator.cleanValue + " "
 executePendingBinaryOperation()
 case .Clear:
 clear()
 }
 }
 }
 
 private func executePendingBinaryOperation(){
 if(pendingBinaryOperation != nil)
 {
 let pending = pendingBinaryOperation!
 _accumulator = pending.pendingOperation(pending.previousOperand, _accumulator)
 pendingBinaryOperation = nil
 }
 }
 
 func clear()
 {
 pendingBinaryOperation = nil
 _accumulator = 0
 _formulaHistory = ""
 }
 
 //Find a way to store the selected BinaryOperation and the current number on screen, until we press another number and then press the equals.
 private struct PendingBinaryOperationInfo
 {
 var pendingOperation: (Double, Double) -> Double
 var previousOperand: Double
 
 }
 
 private enum Operation{
 case Constant(Double)
 case UnaryOperation((Double) -> Double)
 case BinaryOperation((Double, Double) -> Double)
 case Equals
 case Clear
 }
 
 }

 */
